%global pyname mass_prebuild

Name:           mass-prebuild
Version:        1.6.0
Release:        1%{?dist}
Summary:        A set of tools to massively pre-build reverse dependencies for a package

License:        GPL-2.0-or-later
URL:            https://gitlab.com/fedora/packager-tools/mass-prebuild
Source0:        %{url}/-/archive/v%{version}/%{name}-v%{version}.tar.gz

# setuptools_scm doesn't work properly on EPEL8
Patch100:       drop-setuptools_scm.patch

BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%if ! 0%{?el8}
BuildRequires:  python3-setuptools_scm
%endif
BuildRequires:  python3-copr
BuildRequires:  python3-dnf
BuildRequires:  python3-koji
BuildRequires:  python3-pip
BuildRequires:  python3-filelock
BuildRequires:  python3-pyyaml
BuildRequires:  python3-argcomplete
BuildRequires:  python3-daemon
BuildRequires:  rubygem(asciidoctor)
BuildRequires:  make
BuildRequires:  fedpkg
BuildRequires:  mock

Requires: copr-cli
Requires: (python3-libdnf or python3-libdnf5)
Requires: python3-filelock
Requires: python3-PyYAML
Requires: python3-koji
Requires: python3-setuptools_scm
Requires: python3-argcomplete
Requires: python3-daemon
Requires: fedpkg
Requires: mock

Provides: mpb = %{version}-%{release}
Provides: mpb-whatrequires = %{version}-%{release}
Provides: mpb-failedconf = %{version}-%{release}
Provides: mpb-copr-edit = %{version}-%{release}
Provides: mpb-report = %{version}-%{release}


%description
The mass pre-builder tool is a set of helper tools that for the user to
build a package using COPR, calculate a list of reverse dependencies for this
package, build these reverse dependencies against the new version of the main
package and give a report on failures to be manually assessed.


%prep
%autosetup -N -n %{name}-v%{version}
%autopatch -p1 -M 99
%if 0%{?el8}
%autopatch -p1 -m 100
%endif


%build
%py3_build
mkdir -p bash_completion.d
echo "$(register-python-argcomplete mpb)" > bash_completion.d/mpb.bash
echo "$(register-python-argcomplete mpb-copr-edit)" > bash_completion.d/mpb-copr-edit.bash
echo "$(register-python-argcomplete mpb-failedconf)" > bash_completion.d/mpb-failedconf.bash
echo "$(register-python-argcomplete mpb-report)" > bash_completion.d/mpb-report.bash
echo "$(register-python-argcomplete mpb-whatrequires)" > bash_completion.d/mpb-whatrequires.bash
cd man
make
cd -


%install
%py3_install
install -d %{buildroot}%{_sysconfdir}/bash_completion.d
install -pm0644 bash_completion.d/* %{buildroot}%{_sysconfdir}/bash_completion.d
mkdir -p %{buildroot}%{_sysconfdir}/mpb
cp -r examples/*.conf.d %{buildroot}%{_sysconfdir}/mpb
mkdir -p %{buildroot}%{_mandir}/man1
cp -r man/*.1 %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_mandir}/man5
cp -r man/*.5 %{buildroot}%{_mandir}/man5
mkdir -p %{buildroot}%{_mandir}/man7
cp -r man/*.7 %{buildroot}%{_mandir}/man7


%check



%files
%dir %{_sysconfdir}/mpb
%config(noreplace) %{_sysconfdir}/mpb/*.conf.d/*
%config(noreplace) %{_sysconfdir}/bash_completion.d/mpb*
%license LICENSE
%doc README.md examples
%{_mandir}/man1/mpb*.1*
%{_mandir}/man5/mpb*.5*
%{_mandir}/man7/mass-prebuild.7*
%{python3_sitelib}/%{pyname}-*.egg-info/
%{python3_sitelib}/%{pyname}/
%{_bindir}/mpb
%{_bindir}/mpb-whatrequires
%{_bindir}/mpb-failedconf
%{_bindir}/mpb-copr-edit
%{_bindir}/mpb-report


%changelog
* Fri Feb 07 2025 Frédéric Bérat <fberat@redhat.com> - 1.6.0-1
- New upstream release

* Mon Dec 02 2024 Frédéric Bérat <fberat@redhat.com> - 1.5.0-4
- Fix patch list
- Decrease log level for some copr failures

* Fri Nov 29 2024 Frédéric Bérat <fberat@redhat.com> - 1.5.0-3
- Fix `--rebuild` command behavior

* Fri Oct 11 2024 Frédéric Bérat <fberat@redhat.com> - 1.5.0-2
- Fix Changelog
- Fix errors in documentation
- Fix copr failure when using gssapi, and improve logging
- Fix c8s/c9s support and improve testing

* Wed Aug 07 2024 Frédéric Bérat <fberat@redhat.com> - 1.5.0-1
- New upstream release

* Mon Jun 10 2024 Frédéric Bérat <fberat@redhat.com> - 1.4.0-3
- Fix centos-stream patch to support all arches.
- Fix CI for mock builds.

* Fri Jun 07 2024 Frédéric Bérat <fberat@redhat.com> - 1.4.0-2
- Update centos-stream configuration file.

* Mon Apr 29 2024 Frédéric Bérat <fberat@redhat.com> - 1.4.0-1
- New upstream release

* Fri Dec 15 2023 Frédéric Bérat <fberat@redhat.com> - 1.3.1-2
- Rebase setuptools patch

* Fri Dec 15 2023 Frederic Berat <fberat@redhat.com> - 1.3.1-1
- New upstream release

* Tue Dec 12 2023 Frederic Berat <fberat@redhat.com> - 1.3.0-1
- New upstream release

* Thu Sep 28 2023 Frederic Berat <fberat@redhat.com> - 1.2.0-1
- New upstream release
- Migrate to SPDX licenses

* Mon Aug 28 2023 Frederic Berat <fberat@redhat.com> - 1.1.0-2
- Require either DNF or DNF5

* Mon Jul 24 2023 Frederic Berat <fberat@redhat.com> - 1.1.0-1
- New upstream release

* Tue Apr 04 2023 Frederic Berat <fberat@redhat.com> - 1.0.0-5
- Fix EPEL8 support

* Tue Apr 04 2023 Frederic Berat <fberat@redhat.com> - 1.0.0-4
- Fix: wake-up checker worker when there is no build to watch

* Wed Mar 29 2023 Frederic Berat <fberat@redhat.com> - 1.0.0-3
- Fix support for checker specific options

* Mon Mar 06 2023 Frederic Berat <fberat@redhat.com> - 1.0.0-2
- Source full path is wrongly stored in source type field (#76)

* Fri Mar 03 2023 Frederic Berat <fberat@redhat.com> - 1.0.0-1
- New upstream release
- Adding new mpb-report tool
- Add man-pages
- Add command completion

* Fri Feb 10 2023 Frederic Berat <fberat@redhat.com> - 0.6.0-2
- Don't use python3.6 incompatible constructs

* Fri Feb 10 2023 Frederic Berat <fberat@redhat.com> - 0.6.0-1
- New upstream release

* Thu Nov 17 2022 Frederic Berat <fberat@redhat.com> - 0.5.0-2
- Fix: Checker build never got started

* Thu Nov 10 2022 Frederic Berat <fberat@redhat.com> - 0.5.0-1
- New upstream release

* Tue Nov 08 2022 Frederic Berat <fberat@redhat.com> - 0.4.0-3
- Fix: Get build by build ID first
- Reset false positive unconfirmed failure due to previous bug

* Thu Nov 03 2022 Frederic Berat <fberat@redhat.com> - 0.4.0-2
- Fix infinite loop.

* Thu Oct 27 2022 Frederic Berat <fberat@redhat.com> - 0.4.0-1
- New upstream release

* Thu Oct 13 2022 Frederic Berat <fberat@redhat.com> - 0.3.0-1
- New upstream release

* Fri Sep 30 2022 Frederic Berat <fberat@redhat.com> - 0.2.0-2
- Hotfix for c9s koji address

* Mon Sep 05 2022 Frederic Berat <fberat@redhat.com> - 0.2.0-1
- New upstream release

* Tue Aug 23 2022 Frederic Berat <fberat@redhat.com> - 0.1.1-2
- Add new koji requirement

* Tue Aug 23 2022 Frederic Berat <fberat@redhat.com> - 0.1.1-1
- New upstream release 0.1.1

* Tue Aug 09 2022 Frederic Berat <fberat@redhat.com> - 0.1.0~alpha1-5
- Fix packaging review

* Mon Aug 01 2022 Frederic Berat <berat.fred@gmail.com> - 0.1.0~alpha1-4
- Adding support for EPEL8

* Mon Aug 01 2022 Frederic Berat <fberat@redhat.com> - 0.1.0~alpha1-3
- rebuilt

* Mon Jul 04 2022 Frederic Berat <fberat@redhat.com> - 0.1.0~alpha1-2
- Adding "mpb-whatrequires" tool in the package
