#!/bin/bash

set -e

if [ $# -lt 1 ]
then
  echo "New version should be provided e.g.: v1.2.3-4"
  exit 1
fi

if [[ ! $1 =~ ^[vV]([0-9]+)\.([0-9]+)\.([0-9]+)-([0-9]) ]]
then
  echo "Given new version should be like v1.2.3-4"
  exit 2
fi

MAJOR=${BASH_REMATCH[1]}
MINOR=${BASH_REMATCH[2]}
REVISION=${BASH_REMATCH[3]}
RELEASE=${BASH_REMATCH[4]}
tag="v${MAJOR}.${MINOR}.${REVISION}-${RELEASE}"
prev_tag=$(git describe --abbrev=0)
prev_tag=${prev_tag::-2}

git tag -a -m "Release ${tag}" ${tag}

sed -i "s/VERSION_INFO = .*/VERSION_INFO = (${MAJOR}, ${MINOR}, ${REVISION})/" \
  $(dirname $0)/../drop-setuptools_scm.patch

echo "mass-prebuild-v${MAJOR}.${MINOR}.${REVISION}-${RELEASE}" > .gitmessage
echo "" >> .gitmessage

if [ ! ${tag::-2} == ${prev_tag} ]
then
  tar -xaf "mass-prebuild-v${MAJOR}.${MINOR}.${REVISION}.tar.gz"
  CHANGELOG="mass-prebuild-v${MAJOR}.${MINOR}.${REVISION}/scripts/summary.txt"

  cat "${CHANGELOG}" >> .gitmessage

  rpmdev-bumpspec -n "${MAJOR}.${MINOR}.${REVISION}" -c "- New upstream release" mass-prebuild.spec
else
  rpmdev-bumpspec -f .changelog mass-prebuild.spec
  cat .changelog >> .gitmessage
fi

git commit -as -F .gitmessage

git tag -d ${tag}
